.MODEL SMALL
.CODE
ORG 100h
tdata: jmp proses
	lusername db,13,10,'Username : $'
	lpassword db,13,10,'Password : $'
	lditerima db,13,10,'Diterima : $'
	lditolak db,13,10,'Ditolak : $'

	vusername db 23,?,23 dup(?)
	vpassword db 23,?,23 dup(?)
proses:
	MOV AH, 09h
	lea dx, lusername
	INT 21h

	MOV AH,0ah
	lea dx, vusername
	INT 21h

	MOV AH, 09h
	lea dx, lpassword
	INT 21h

	MOV AH,0ah
	lea dx, vpassword
	INT 21h

	lea si,vusername
	lea di,vpassword

	cld
	MOV CX, 23
	rep cmpsb
	jne gagal

	MOV AH,09h
	lea dx,lditerima
	INT 21h
	jmp exit

gagal:
	MOV AH, 09h
	lea dx, lditolak
	INT 21h
	jmp proses
exit:
	INT 20h
end tdata